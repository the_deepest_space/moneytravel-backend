-- create schemas
-- backend - business logic
-- api     - facade
CREATE SCHEMA backend;
CREATE SCHEMA api;

GRANT USAGE ON SCHEMA api TO api_user;

-- BACKEND DEFINITION

-- storing basic user info
CREATE TABLE backend.t_users (
  id     SERIAL PRIMARY KEY,
  fb_uid TEXT UNIQUE NOT NULL,
  score  INTEGER DEFAULT(0)
);

CREATE FUNCTION backend.fb_try_create_user(_fb_uid TEXT)
RETURNS INTEGER
LANGUAGE plpgsql AS $$
  DECLARE new_id INTEGER;
  BEGIN
    IF (SELECT COUNT(*)
          FROM backend.t_users u
         WHERE u.fb_uid = _fb_uid) <> 0 THEN
      RETURN (SELECT id
                FROM backend.t_users u
               WHERE u.fb_uid = _fb_uid);
    END IF;

    INSERT INTO backend.t_users(fb_uid)
         VALUES (_fb_uid)
      RETURNING id
           INTO new_id;

    RETURN new_id;
  END;
$$;

-- store basic informations about bills
CREATE TABLE backend.t_banknotes (
  id            SERIAL      PRIMARY KEY,
  serial_number VARCHAR(10) UNIQUE      NOT NULL
);

CREATE FUNCTION backend.fn_banknotes_post_insert()
RETURNS TRIGGER
LANGUAGE plpgsql AS $$
  BEGIN
    PERFORM pg_notify(
      'wslistener',
      json_build_object(
        'msg', 'new bill',
        'data', json_build_object(
          'serial_number', new.serial_number
        )
      )::text
    );
    RETURN new;
  END;
$$;

CREATE TRIGGER tg_banknotes
  AFTER INSERT ON backend.t_banknotes
  FOR EACH ROW
  EXECUTE PROCEDURE backend.fn_banknotes_post_insert();

-- storing check-ins data
CREATE TABLE backend.t_checkins(
  id                 SERIAL       PRIMARY KEY,
  banknote_id        INTEGER      REFERENCES backend.t_banknotes(id) NOT NULL,
  author_id          INTEGER      REFERENCES backend.t_users(id) NOT NULL,
  timestamp          TIMESTAMPTZ  NOT NULL,
  comment            TEXT,
  picture            BYTEA        NOT NULL,
  location_latitude  NUMERIC(6,4) NOT NULL,
  location_longitude NUMERIC(7,4) NOT NULL,
  location_text      TEXT         NOT NULL,
  score              INTEGER      NOT NULL DEFAULT(0)

  CHECK (timestamp <= NOW()),
  CHECK (SUBSTRING(picture FROM 1 FOR 4) = E'\\x89504E47'),
  CHECK (location_latitude >= -90 AND location_latitude <= 90),
  CHECK (location_longitude >= -180 AND location_longitude <= 180)
);

CREATE FUNCTION backend.fn_checkins_post_insert()
RETURNS TRIGGER
LANGUAGE plpgsql AS $$
  BEGIN
    PERFORM pg_notify(
      'wslistener',
      json_build_object(
        'msg', 'new checkin',
        'data', json_build_object(
          'id', new.id,
          'timestamp', new.timestamp,
          'serial_number', (SELECT serial_number
                              FROM backend.t_banknotes
                             WHERE id = new.banknote_id),
          'lat', new.location_latitude,
          'lng', new.location_longitude
        )
      )::text
    );
    RETURN new;
  END;
$$;

CREATE TRIGGER tg_checkins
  AFTER INSERT ON backend.t_checkins
  FOR EACH ROW
  EXECUTE PROCEDURE backend.fn_checkins_post_insert();

-- performing a check-in of a new of existing bill
CREATE FUNCTION backend.fn_checkin(
  author_id_ INTEGER,
  serial_number_ VARCHAR(7),
  timestamp_ TIMESTAMPTZ,
  comment TEXT,
  picture BYTEA,
  location_latitude NUMERIC(6,4),
  location_longitude NUMERIC(7,4),
  location_text TEXT
) RETURNS BOOLEAN LANGUAGE plpgsql AS $$
  DECLARE
    banknote_id INTEGER;
    checkin_id INTEGER;
  BEGIN
    IF (SELECT COUNT(*)
          FROM backend.t_banknotes b
         WHERE b.serial_number = serial_number_) = 0 THEN
      INSERT INTO backend.t_banknotes (serial_number) VALUES (serial_number_)
      RETURNING id INTO banknote_id;
    ELSE
      SELECT id
        FROM backend.t_banknotes b
       WHERE b.serial_number = serial_number_
        INTO banknote_id;
    END IF;

    INSERT INTO backend.t_checkins (
      banknote_id,
      author_id,
      timestamp,
      comment,
      picture,
      location_latitude,
      location_longitude,
      location_text
    ) VALUES (
      banknote_id,
      author_id_,
      timestamp_,
      comment,
      picture,
      location_latitude,
      location_longitude,
      location_text
    ) RETURNING id INTO checkin_id;

    RETURN (checkin_id IS NOT NULL);
  END;
$$;

-- API DEFINITION

-- user creation procedure wrapper
CREATE FUNCTION api.try_create_user(_fb_uid TEXT)
RETURNS INTEGER
SECURITY DEFINER
LANGUAGE plpgsql AS $$
  DECLARE id INTEGER;
  BEGIN
    SELECT backend.fb_try_create_user(_fb_uid)
      INTO id;
    RETURN id;
  END;
$$;

GRANT EXECUTE ON FUNCTION api.try_create_user TO api_user;

-- viewing checkins
CREATE VIEW api.checkins AS
  SELECT ci.id AS id
       , b.id AS banknote_id
       , b.serial_number AS serial_number
       , ci.timestamp AS timestamp
       , to_char(ci.timestamp, 'YYYY-MM-DD') AS pretty_timestamp
       , encode(ci.picture, 'base64') AS picture
       , ci.comment AS comment
       , ci.location_latitude AS location_latitude
       , ci.location_longitude AS location_longitude
       , ci.location_text AS location_text
       , ci.score AS score
       , u.fb_uid AS author_id
    FROM backend.t_checkins ci
    JOIN backend.t_banknotes b
      ON ci.banknote_id = b.id
    JOIN backend.t_users u
      ON ci.author_id = u.id;

GRANT SELECT ON api.checkins TO api_user;

-- viewing bill profiles
CREATE VIEW api.banknote_profiles AS
  WITH profiles AS (
      SELECT b.id AS id
           , b.serial_number AS serial_number
           , json_agg((ci.*) ORDER BY ci.timestamp DESC) AS checkins
        FROM backend.t_banknotes b
        JOIN api.checkins ci ON b.id = ci.banknote_id
    GROUP BY b.id, b.serial_number
  ) SELECT p.id
         , p.serial_number
         , p.checkins
         , ci1.timestamp        AS first_appearence_timestamp
         , ci1.picture          AS avatar_picture
         , ci1.location_text    AS first_appearance_location
         , ci1.author_id        AS first_author_id
      FROM profiles p
      JOIN api.checkins ci1
        ON ci1.id = (
               SELECT id
                 FROM api.checkins tmp
                WHERE tmp.banknote_id = p.id
             ORDER BY timestamp ASC
                LIMIT 1
           )
  ORDER BY p.serial_number;

GRANT SELECT ON api.banknote_profiles TO api_user;

-- checkin function wrapper
CREATE FUNCTION api.checkin(
  serial_number VARCHAR(7),
  timestamp_ TEXT,
  comment TEXT,
  picture TEXT,
  location_latitude NUMERIC(6,4),
  location_longitude NUMERIC(7,4),
  location_text TEXT
) RETURNS BOOLEAN
  LANGUAGE plpgsql
  SECURITY DEFINER AS $$
  DECLARE result BOOLEAN;
  BEGIN
    SELECT backend.fn_checkin(
      current_setting('api.user_id')::INTEGER,
      serial_number,
      timestamp_::timestamptz,
      comment,
      decode(picture, 'base64')::bytea,
      location_latitude,
      location_longitude,
      location_text
    ) INTO result;

    RETURN result;
  END
$$;

GRANT EXECUTE ON FUNCTION api.checkin TO api_user;

-- bill location summary
CREATE VIEW api.banknote_locations AS
    SELECT b.id AS id
         , b.serial_number AS serial_number
         , json_agg(
             json_build_object(
               'lat', ci.location_latitude,
               'lng', ci.location_longitude,
               'timestamp', ci.timestamp
             ) ORDER BY ci.timestamp ASC
           ) AS locations
      FROM backend.t_banknotes b
      JOIN api.checkins ci
        ON b.id = ci.banknote_id
  GROUP BY b.id, b.serial_number;

GRANT SELECT ON api.banknote_locations TO api_user;

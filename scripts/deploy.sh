prod_host=$PRODUCTION_HOST
prod_user=$PRODUCTION_USER
prod_workdir=$PRODUCTION_WORKDIR

api_files='api/api.js api/package.json api/package-lock.json api/service-key.json'
db_files='db'
other_files='.env docker-compose.yml'

start_stage() {
  echo "! --> $1"
}
report() {
  local ec=`echo $?`
  if [ $ec -eq 0  ]
  then
    echo '! --> success!'
  else
    echo "! --> exit code: $ec"
  fi
}
deploy() {
  start_stage 'preparing server'
  ssh $prod_user@$prod_host mkdir -p $prod_workdir/{api,db}
  report

  start_stage 'copying required files'
  scp $other_files $prod_user@$prod_host:$prod_workdir
  scp $api_files $prod_user@$prod_host:$prod_workdir/api
  scp -r $db_files $prod_user@$prod_host:$prod_workdir
  report

  start_stage 'starting containers'
  ssh $prod_user@$prod_host "
    cd $prod_workdir &&
    sudo docker-compose down &&
    sudo docker-compose up -d
  "
  report
}
deploy
